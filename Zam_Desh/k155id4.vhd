----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:37:32 02/19/2016 
-- Design Name: 
-- Module Name:    k155id4 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity k155id4 is
    Port ( EA : in  STD_LOGIC;
           EAI : in  STD_LOGIC;
				EB : in  STD_LOGIC;
           EBI : in  STD_LOGIC;
			  A : in  STD_LOGIC_VECTOR (1 downto 0);
           y : out  STD_LOGIC_VECTOR (7 downto 0));
end k155id4;

architecture Behavioral of k155id4 is

signal EAA :   STD_LOGIC;
signal EBA :   STD_LOGIC;
signal DOUT :   STD_LOGIC_VECTOR (7 downto 0);

begin

	process(A, EA, EAI)
	begin		
			if (EA = '1' and EAI = '0') then
				case A is
					when "00" =>   dout(3 downto 0) <= "1110";
					when "01" =>   dout(3 downto 0) <= "1101";
					when "10" =>   dout(3 downto 0) <= "1011";
					when "11" =>   dout(3 downto 0) <= "0111";
					when others => dout(3 downto 0) <= "1111";
				end case;
			else 
				dout(3 downto 0) <= "1111";
			end if;
	end process;
	
	process(A, EB, EBI)
	begin		
			if (EB = '0' and EBI = '0') then
				case A is
					when "00" =>   dout(7 downto 4) <= "1110";
					when "01" =>   dout(7 downto 4) <= "1101";
					when "10" =>   dout(7 downto 4) <= "1011";
					when "11" =>   dout(7 downto 4) <= "0111";
					when others => dout(7 downto 4) <= "1111";
				end case;
			else 
				dout(7 downto 4) <= "1111";
			end if;
	end process;
	
	y<=dout;	
	
end Behavioral;

