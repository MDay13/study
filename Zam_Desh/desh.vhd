----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:19:53 02/26/2016 
-- Design Name: 
-- Module Name:    desh - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity desh is
    Port ( E : in  STD_LOGIC;
           A : in  STD_LOGIC_VECTOR (1 downto 0);
          -- CLK : in  STD_LOGIC;
           Y : out  STD_LOGIC_VECTOR (3 downto 0));
end desh;

architecture Behavioral of desh is

signal dout : std_logic_vector (1 downto 0);

begin

	process(CLK)
	begin
		if (CLK = '1' ) then
				dout <= A;
		end if;
	end process;
	
	with dout Select 
	Y  <=  "0001" when "00",
		"0010" when "01",
		"0100" when "10",
		"1000" when "11",
		"0000" when others;

end Behavioral;

