--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   01:47:45 03/18/2016
-- Design Name:   
-- Module Name:   D:/Project/Zam_Desh/id3TB.vhd
-- Project Name:  Zam_Desh
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: k155id3
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY id3TB IS
END id3TB;
 
ARCHITECTURE behavior OF id3TB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT k155id3
    PORT(
         A0 : IN  std_logic;
         A1 : IN  std_logic;
         A2 : IN  std_logic;
         A3 : IN  std_logic;
         E0 : IN  std_logic;
         E1 : IN  std_logic;
         Q : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal A0 : std_logic := '0';
   signal A1 : std_logic := '0';
   signal A2 : std_logic := '0';
   signal A3 : std_logic := '0';
   signal E0 : std_logic := '0';
   signal E1 : std_logic := '0';

 	--Outputs
   signal Q : std_logic_vector(15 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
	signal CLK : std_logic;
 
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: k155id3 PORT MAP (
          A0 => A0,
          A1 => A1,
          A2 => A2,
          A3 => A3,
          E0 => E0,
          E1 => E1,
          Q => Q
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 
		
		A0 <= '1';
		A1 <= '0';
		wait for CLK_period*10;
		
		A0 <= '1';
		A1 <= '1';
		wait for CLK_period*10;
		
		A0 <= '0';
		A1 <= '0';
		A2 <= '1';
		wait for CLK_period*10;

      wait;
   end process;

END;
