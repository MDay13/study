--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   01:04:55 03/18/2016
-- Design Name:   
-- Module Name:   D:/Project/Zam_Desh/CascadeTBPart1.vhd
-- Project Name:  Zam_Desh
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: k155id4Cascade
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY CascadeTBPart1 IS
END CascadeTBPart1;
 
ARCHITECTURE behavior OF CascadeTBPart1 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT k155id4Cascade
    PORT(
         EA : IN  std_logic;
         EAI : IN  std_logic;
         EB : IN  std_logic;
         EBI : IN  std_logic;
         A : IN  std_logic_vector(5 downto 0);
         y : OUT  std_logic_vector(127 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal EA : std_logic := '0';
   signal EAI : std_logic := '0';
   signal EB : std_logic := '0';
   signal EBI : std_logic := '0';
   signal A : std_logic_vector(5 downto 0) := (others => '0');

 	--Outputs
   signal y : std_logic_vector(127 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
	signal CLK : std_logic;
 
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: k155id4Cascade PORT MAP (
          EA => EA,
          EAI => EAI,
          EB => EB,
          EBI => EBI,
          A => A,
          y => y
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 
		
		EA <= '1';
		EAI <= '0';
		
	-------------------------------------------
		wait for CLK_period*5;
		A <= "000001";
		wait for CLK_period*5;
		A <= "000001";
		wait for CLK_period*5;
		A <= "000010";
		wait for CLK_period*5;
		A <= "000011";
		
		wait for CLK_period*5;
		A <= "000101";
		wait for CLK_period*5;
		A <= "000101";
		wait for CLK_period*5;
		A <= "000110";
		wait for CLK_period*5;
		A <= "000111";
		
      wait;
   end process;

END;
