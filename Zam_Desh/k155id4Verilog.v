`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:58:09 03/17/2016 
// Design Name: 
// Module Name:    k155id4Verilog 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module k155id4Verilog(
    input EA,
    input EAI,
    input EB,
    input EBI,
    input [1:0] A,
    output reg [7:0] Y
    );

	always @(EA or EAI or A) begin
	if (EA & ~EAI)
		case (A)
		   2'b00 : Y = 8'b11111110;
			2'b01 : Y = 8'b11111101;
			2'b10 : Y = 8'b11111011;
			2'b11 : Y = 8'b11110111;
			default : Y = 8'b11111111;
		endcase 
	else
		Y = 8'b11111111;
	end

endmodule



