----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:58:15 03/11/2016 
-- Design Name: 
-- Module Name:    k155id4Cascade - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity k155id4Cascade is
port
(
---------------------------------------------
		EA : in  STD_LOGIC;
      EAI : in  STD_LOGIC;
		EB : in  STD_LOGIC;
      EBI : in  STD_LOGIC;
		A : in  STD_LOGIC_VECTOR (5 downto 0);
      y : out  STD_LOGIC_VECTOR (127 downto 0)
		
---------------------------------------------
);
end k155id4Cascade;

architecture Behavioral of k155id4Cascade is

component k155id4
port
(
---------------------------------------------
		EA : in  STD_LOGIC;
      EAI : in  STD_LOGIC;
		EB : in  STD_LOGIC;
      EBI : in  STD_LOGIC;
		A : in  STD_LOGIC_VECTOR (1 downto 0);
      y : out  STD_LOGIC_VECTOR (7 downto 0)
---------------------------------------------
);
end component;

component k155id3
port
(
---------------------------------------------
		  A0 : in STD_LOGIC;
		  A1 : in STD_LOGIC;
		  A2 : in STD_LOGIC;
		  A3 : in STD_LOGIC;
		  E0 : in STD_LOGIC;
		  E1 : in STD_LOGIC;
		  Q  : out STD_LOGIC_VECTOR (15 downto 0)
---------------------------------------------
);

end component;

signal out1 : STD_LOGIC_VECTOR (7 downto 0);

begin
MAIN:k155id4
port map
(
	EA => EA,
	EAI => EAI,
	EB => EB,
	EBI => EBI,
	A => A(5 downto 4),
	
	y => out1
);

--------------------------------------------

INNER_1_1:k155id3
port map
(
	A0 => A(0),
	A1 => A(1),
	A2 => A(2),
	A3 => A(3),
	E0 => out1(0),
	E1 => EAI,
	
	Q  => y(15 downto 0)
);

INNER_1_2:k155id3
port map
(
	A0 => A(0),
	A1 => A(1),
	A2 => A(2),
	A3 => A(3),
	E0 => out1(1),
	E1 => EAI,
	
	Q  => y(31 downto 16)
);

INNER_1_3:k155id3
port map
(
	A0 => A(0),
	A1 => A(1),
	A2 => A(2),
	A3 => A(3),
	E0 => out1(2),
	E1 => EAI,
	
	Q  => y(47 downto 32)
);

INNER_1_4:k155id3
port map
(
	A0 => A(0),
	A1 => A(1),
	A2 => A(2),
	A3 => A(3),
	E0 => out1(3),
	E1 => EAI,
	
	Q  => y(63 downto 48)
);

--------------------------------------------

INNER_2_1:k155id3
port map
(
	A0 => A(0),
	A1 => A(1),
	A2 => A(2),
	A3 => A(3),
	E0 => out1(4),
	E1 => EBI,
	
	Q  => y(79 downto 64)
);

INNER_2_2:k155id3
port map
(
	A0 => A(0),
	A1 => A(1),
	A2 => A(2),
	A3 => A(3),
	E0 => out1(5),
	E1 => EBI,
	
	Q  => y(95 downto 80)
);

INNER_2_3:k155id3
port map
(
	A0 => A(0),
	A1 => A(1),
	A2 => A(2),
	A3 => A(3),
	E0 => out1(6),
	E1 => EBI,
	
	Q  => y(111 downto 96)
);

INNER_2_4:k155id3
port map
(
	A0 => A(0),
	A1 => A(1),
	A2 => A(2),
	A3 => A(3),
	E0 => out1(7),
	E1 => EBI,
	
	Q  => y(127 downto 112)
);

end Behavioral;

