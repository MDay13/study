`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   00:12:59 03/18/2016
// Design Name:   k155id4Verilog
// Module Name:   D:/Project/Zam_Desh/k155id4Test.v
// Project Name:  Zam_Desh
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: k155id4Verilog
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module k155id4Test;

	// Inputs
	reg EA;
	reg EAI;
	reg EB;
	reg EBI;
	reg [1:0] A;

	// Outputs
	wire [7:0] Y;

	// Instantiate the Unit Under Test (UUT)
	k155id4Verilog uut (
		.EA(EA), 
		.EAI(EAI), 
		.EB(EB), 
		.EBI(EBI), 
		.A(A), 
		.Y(Y)
	);

	initial begin
		// Initialize Inputs
		EA = 0;
		EAI = 0;
		EB = 0;
		EBI = 0;
		A = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		
		#100
		EA =1;
		A=2'b00;
		
		#100
		A=2'b01;
		
		#100
		A=2'b11;
		

	end
      
endmodule

