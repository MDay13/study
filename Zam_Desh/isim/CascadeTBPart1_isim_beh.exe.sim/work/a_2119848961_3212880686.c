/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x2f00eba5 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/Project/OgulenkoVerilog/k155id3/k155id3.vhd";
extern char *IEEE_P_2592010699;

char *ieee_p_2592010699_sub_1837678034_503743352(char *, char *, char *, char *);


static void work_a_2119848961_3212880686_p_0(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    unsigned char t7;
    unsigned char t8;
    unsigned char t9;
    unsigned char t10;
    unsigned char t11;
    char *t12;
    unsigned char t13;
    unsigned char t14;
    char *t15;
    unsigned char t16;
    unsigned char t17;
    char *t18;
    unsigned char t19;
    unsigned char t20;
    char *t21;
    unsigned char t22;
    unsigned char t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;

LAB0:    xsi_set_current_line(48, ng0);
    t2 = (t0 + 960U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)2);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(83, ng0);
    t2 = (t0 + 4102);
    t6 = (t0 + 2288);
    t12 = (t6 + 32U);
    t15 = *((char **)t12);
    t18 = (t15 + 40U);
    t21 = *((char **)t18);
    memcpy(t21, t2, 16U);
    xsi_driver_first_trans_fast(t6);

LAB3:    t2 = (t0 + 2236);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(49, ng0);
    t2 = (t0 + 592U);
    t12 = *((char **)t2);
    t13 = *((unsigned char *)t12);
    t14 = (t13 == (unsigned char)2);
    if (t14 == 1)
        goto LAB17;

LAB18:    t11 = (unsigned char)0;

LAB19:    if (t11 == 1)
        goto LAB14;

LAB15:    t10 = (unsigned char)0;

LAB16:    if (t10 == 1)
        goto LAB11;

LAB12:    t9 = (unsigned char)0;

LAB13:    if (t9 != 0)
        goto LAB8;

LAB10:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)3);
    if (t8 == 1)
        goto LAB28;

LAB29:    t5 = (unsigned char)0;

LAB30:    if (t5 == 1)
        goto LAB25;

LAB26:    t4 = (unsigned char)0;

LAB27:    if (t4 == 1)
        goto LAB22;

LAB23:    t1 = (unsigned char)0;

LAB24:    if (t1 != 0)
        goto LAB20;

LAB21:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)2);
    if (t8 == 1)
        goto LAB39;

LAB40:    t5 = (unsigned char)0;

LAB41:    if (t5 == 1)
        goto LAB36;

LAB37:    t4 = (unsigned char)0;

LAB38:    if (t4 == 1)
        goto LAB33;

LAB34:    t1 = (unsigned char)0;

LAB35:    if (t1 != 0)
        goto LAB31;

LAB32:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)3);
    if (t8 == 1)
        goto LAB50;

LAB51:    t5 = (unsigned char)0;

LAB52:    if (t5 == 1)
        goto LAB47;

LAB48:    t4 = (unsigned char)0;

LAB49:    if (t4 == 1)
        goto LAB44;

LAB45:    t1 = (unsigned char)0;

LAB46:    if (t1 != 0)
        goto LAB42;

LAB43:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)2);
    if (t8 == 1)
        goto LAB61;

LAB62:    t5 = (unsigned char)0;

LAB63:    if (t5 == 1)
        goto LAB58;

LAB59:    t4 = (unsigned char)0;

LAB60:    if (t4 == 1)
        goto LAB55;

LAB56:    t1 = (unsigned char)0;

LAB57:    if (t1 != 0)
        goto LAB53;

LAB54:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)3);
    if (t8 == 1)
        goto LAB72;

LAB73:    t5 = (unsigned char)0;

LAB74:    if (t5 == 1)
        goto LAB69;

LAB70:    t4 = (unsigned char)0;

LAB71:    if (t4 == 1)
        goto LAB66;

LAB67:    t1 = (unsigned char)0;

LAB68:    if (t1 != 0)
        goto LAB64;

LAB65:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)2);
    if (t8 == 1)
        goto LAB83;

LAB84:    t5 = (unsigned char)0;

LAB85:    if (t5 == 1)
        goto LAB80;

LAB81:    t4 = (unsigned char)0;

LAB82:    if (t4 == 1)
        goto LAB77;

LAB78:    t1 = (unsigned char)0;

LAB79:    if (t1 != 0)
        goto LAB75;

LAB76:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)3);
    if (t8 == 1)
        goto LAB94;

LAB95:    t5 = (unsigned char)0;

LAB96:    if (t5 == 1)
        goto LAB91;

LAB92:    t4 = (unsigned char)0;

LAB93:    if (t4 == 1)
        goto LAB88;

LAB89:    t1 = (unsigned char)0;

LAB90:    if (t1 != 0)
        goto LAB86;

LAB87:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)2);
    if (t8 == 1)
        goto LAB105;

LAB106:    t5 = (unsigned char)0;

LAB107:    if (t5 == 1)
        goto LAB102;

LAB103:    t4 = (unsigned char)0;

LAB104:    if (t4 == 1)
        goto LAB99;

LAB100:    t1 = (unsigned char)0;

LAB101:    if (t1 != 0)
        goto LAB97;

LAB98:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)3);
    if (t8 == 1)
        goto LAB116;

LAB117:    t5 = (unsigned char)0;

LAB118:    if (t5 == 1)
        goto LAB113;

LAB114:    t4 = (unsigned char)0;

LAB115:    if (t4 == 1)
        goto LAB110;

LAB111:    t1 = (unsigned char)0;

LAB112:    if (t1 != 0)
        goto LAB108;

LAB109:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)2);
    if (t8 == 1)
        goto LAB127;

LAB128:    t5 = (unsigned char)0;

LAB129:    if (t5 == 1)
        goto LAB124;

LAB125:    t4 = (unsigned char)0;

LAB126:    if (t4 == 1)
        goto LAB121;

LAB122:    t1 = (unsigned char)0;

LAB123:    if (t1 != 0)
        goto LAB119;

LAB120:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)3);
    if (t8 == 1)
        goto LAB138;

LAB139:    t5 = (unsigned char)0;

LAB140:    if (t5 == 1)
        goto LAB135;

LAB136:    t4 = (unsigned char)0;

LAB137:    if (t4 == 1)
        goto LAB132;

LAB133:    t1 = (unsigned char)0;

LAB134:    if (t1 != 0)
        goto LAB130;

LAB131:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)2);
    if (t8 == 1)
        goto LAB149;

LAB150:    t5 = (unsigned char)0;

LAB151:    if (t5 == 1)
        goto LAB146;

LAB147:    t4 = (unsigned char)0;

LAB148:    if (t4 == 1)
        goto LAB143;

LAB144:    t1 = (unsigned char)0;

LAB145:    if (t1 != 0)
        goto LAB141;

LAB142:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)3);
    if (t8 == 1)
        goto LAB160;

LAB161:    t5 = (unsigned char)0;

LAB162:    if (t5 == 1)
        goto LAB157;

LAB158:    t4 = (unsigned char)0;

LAB159:    if (t4 == 1)
        goto LAB154;

LAB155:    t1 = (unsigned char)0;

LAB156:    if (t1 != 0)
        goto LAB152;

LAB153:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)2);
    if (t8 == 1)
        goto LAB171;

LAB172:    t5 = (unsigned char)0;

LAB173:    if (t5 == 1)
        goto LAB168;

LAB169:    t4 = (unsigned char)0;

LAB170:    if (t4 == 1)
        goto LAB165;

LAB166:    t1 = (unsigned char)0;

LAB167:    if (t1 != 0)
        goto LAB163;

LAB164:    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t7 = *((unsigned char *)t3);
    t8 = (t7 == (unsigned char)3);
    if (t8 == 1)
        goto LAB182;

LAB183:    t5 = (unsigned char)0;

LAB184:    if (t5 == 1)
        goto LAB179;

LAB180:    t4 = (unsigned char)0;

LAB181:    if (t4 == 1)
        goto LAB176;

LAB177:    t1 = (unsigned char)0;

LAB178:    if (t1 != 0)
        goto LAB174;

LAB175:
LAB9:    goto LAB3;

LAB5:    t2 = (t0 + 1052U);
    t6 = *((char **)t2);
    t7 = *((unsigned char *)t6);
    t8 = (t7 == (unsigned char)2);
    t1 = t8;
    goto LAB7;

LAB8:    xsi_set_current_line(50, ng0);
    t2 = (t0 + 3846);
    t25 = (t0 + 2288);
    t26 = (t25 + 32U);
    t27 = *((char **)t26);
    t28 = (t27 + 40U);
    t29 = *((char **)t28);
    memcpy(t29, t2, 16U);
    xsi_driver_first_trans_fast(t25);
    goto LAB9;

LAB11:    t2 = (t0 + 868U);
    t21 = *((char **)t2);
    t22 = *((unsigned char *)t21);
    t23 = (t22 == (unsigned char)2);
    t9 = t23;
    goto LAB13;

LAB14:    t2 = (t0 + 776U);
    t18 = *((char **)t2);
    t19 = *((unsigned char *)t18);
    t20 = (t19 == (unsigned char)2);
    t10 = t20;
    goto LAB16;

LAB17:    t2 = (t0 + 684U);
    t15 = *((char **)t2);
    t16 = *((unsigned char *)t15);
    t17 = (t16 == (unsigned char)2);
    t11 = t17;
    goto LAB19;

LAB20:    xsi_set_current_line(52, ng0);
    t2 = (t0 + 3862);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB22:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)2);
    t1 = t16;
    goto LAB24;

LAB25:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)2);
    t4 = t13;
    goto LAB27;

LAB28:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)2);
    t5 = t10;
    goto LAB30;

LAB31:    xsi_set_current_line(54, ng0);
    t2 = (t0 + 3878);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB33:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)2);
    t1 = t16;
    goto LAB35;

LAB36:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)2);
    t4 = t13;
    goto LAB38;

LAB39:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)3);
    t5 = t10;
    goto LAB41;

LAB42:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 3894);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB44:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)2);
    t1 = t16;
    goto LAB46;

LAB47:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)2);
    t4 = t13;
    goto LAB49;

LAB50:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)3);
    t5 = t10;
    goto LAB52;

LAB53:    xsi_set_current_line(58, ng0);
    t2 = (t0 + 3910);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB55:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)2);
    t1 = t16;
    goto LAB57;

LAB58:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)2);
    t4 = t13;
    goto LAB60;

LAB61:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)2);
    t5 = t10;
    goto LAB63;

LAB64:    xsi_set_current_line(60, ng0);
    t2 = (t0 + 3926);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB66:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)2);
    t1 = t16;
    goto LAB68;

LAB69:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)3);
    t4 = t13;
    goto LAB71;

LAB72:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)2);
    t5 = t10;
    goto LAB74;

LAB75:    xsi_set_current_line(62, ng0);
    t2 = (t0 + 3942);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB77:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)2);
    t1 = t16;
    goto LAB79;

LAB80:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)3);
    t4 = t13;
    goto LAB82;

LAB83:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)3);
    t5 = t10;
    goto LAB85;

LAB86:    xsi_set_current_line(64, ng0);
    t2 = (t0 + 3958);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB88:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)2);
    t1 = t16;
    goto LAB90;

LAB91:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)3);
    t4 = t13;
    goto LAB93;

LAB94:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)3);
    t5 = t10;
    goto LAB96;

LAB97:    xsi_set_current_line(66, ng0);
    t2 = (t0 + 3974);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB99:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)3);
    t1 = t16;
    goto LAB101;

LAB102:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)3);
    t4 = t13;
    goto LAB104;

LAB105:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)2);
    t5 = t10;
    goto LAB107;

LAB108:    xsi_set_current_line(68, ng0);
    t2 = (t0 + 3990);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB110:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)3);
    t1 = t16;
    goto LAB112;

LAB113:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)2);
    t4 = t13;
    goto LAB115;

LAB116:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)2);
    t5 = t10;
    goto LAB118;

LAB119:    xsi_set_current_line(70, ng0);
    t2 = (t0 + 4006);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB121:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)3);
    t1 = t16;
    goto LAB123;

LAB124:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)2);
    t4 = t13;
    goto LAB126;

LAB127:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)3);
    t5 = t10;
    goto LAB129;

LAB130:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 4022);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB132:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)3);
    t1 = t16;
    goto LAB134;

LAB135:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)2);
    t4 = t13;
    goto LAB137;

LAB138:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)3);
    t5 = t10;
    goto LAB140;

LAB141:    xsi_set_current_line(74, ng0);
    t2 = (t0 + 4038);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB143:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)3);
    t1 = t16;
    goto LAB145;

LAB146:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)3);
    t4 = t13;
    goto LAB148;

LAB149:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)2);
    t5 = t10;
    goto LAB151;

LAB152:    xsi_set_current_line(76, ng0);
    t2 = (t0 + 4054);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB154:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)3);
    t1 = t16;
    goto LAB156;

LAB157:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)3);
    t4 = t13;
    goto LAB159;

LAB160:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)2);
    t5 = t10;
    goto LAB162;

LAB163:    xsi_set_current_line(78, ng0);
    t2 = (t0 + 4070);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB165:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)3);
    t1 = t16;
    goto LAB167;

LAB168:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)3);
    t4 = t13;
    goto LAB170;

LAB171:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)3);
    t5 = t10;
    goto LAB173;

LAB174:    xsi_set_current_line(80, ng0);
    t2 = (t0 + 4086);
    t21 = (t0 + 2288);
    t24 = (t21 + 32U);
    t25 = *((char **)t24);
    t26 = (t25 + 40U);
    t27 = *((char **)t26);
    memcpy(t27, t2, 16U);
    xsi_driver_first_trans_fast(t21);
    goto LAB9;

LAB176:    t2 = (t0 + 868U);
    t15 = *((char **)t2);
    t14 = *((unsigned char *)t15);
    t16 = (t14 == (unsigned char)3);
    t1 = t16;
    goto LAB178;

LAB179:    t2 = (t0 + 776U);
    t12 = *((char **)t2);
    t11 = *((unsigned char *)t12);
    t13 = (t11 == (unsigned char)3);
    t4 = t13;
    goto LAB181;

LAB182:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)3);
    t5 = t10;
    goto LAB184;

}

static void work_a_2119848961_3212880686_p_1(char *t0)
{
    char t1[16];
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    unsigned int t6;
    unsigned int t7;
    unsigned char t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;

LAB0:    xsi_set_current_line(88, ng0);

LAB3:    t2 = (t0 + 1236U);
    t3 = *((char **)t2);
    t2 = (t0 + 3800U);
    t4 = ieee_p_2592010699_sub_1837678034_503743352(IEEE_P_2592010699, t1, t3, t2);
    t5 = (t1 + 12U);
    t6 = *((unsigned int *)t5);
    t7 = (1U * t6);
    t8 = (16U != t7);
    if (t8 == 1)
        goto LAB5;

LAB6:    t9 = (t0 + 2324);
    t10 = (t9 + 32U);
    t11 = *((char **)t10);
    t12 = (t11 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t4, 16U);
    xsi_driver_first_trans_fast_port(t9);

LAB2:    t14 = (t0 + 2244);
    *((int *)t14) = 1;

LAB1:    return;
LAB4:    goto LAB2;

LAB5:    xsi_size_not_matching(16U, t7, 0);
    goto LAB6;

}


extern void work_a_2119848961_3212880686_init()
{
	static char *pe[] = {(void *)work_a_2119848961_3212880686_p_0,(void *)work_a_2119848961_3212880686_p_1};
	xsi_register_didat("work_a_2119848961_3212880686", "isim/CascadeTBPart1_isim_beh.exe.sim/work/a_2119848961_3212880686.didat");
	xsi_register_executes(pe);
}
