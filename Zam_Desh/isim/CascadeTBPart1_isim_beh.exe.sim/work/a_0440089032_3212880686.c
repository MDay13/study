/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x2f00eba5 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "D:/Project/Zam_Desh/k155id4.vhd";



static void work_a_0440089032_3212880686_p_0(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    unsigned char t7;
    unsigned char t8;
    char *t9;
    char *t10;
    int t11;
    char *t12;
    char *t13;
    int t14;
    char *t15;
    int t17;
    char *t18;
    int t20;
    char *t21;
    unsigned char t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;

LAB0:    xsi_set_current_line(51, ng0);
    t2 = (t0 + 592U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)3);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(60, ng0);
    t2 = (t0 + 4048);
    t1 = (4U != 4U);
    if (t1 == 1)
        goto LAB29;

LAB30:    t6 = (t0 + 2532);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_delta(t6, 4U, 4U, 0LL);

LAB3:    t2 = (t0 + 2472);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(52, ng0);
    t2 = (t0 + 960U);
    t9 = *((char **)t2);
    t2 = (t0 + 4020);
    t11 = xsi_mem_cmp(t2, t9, 2U);
    if (t11 == 1)
        goto LAB9;

LAB14:    t12 = (t0 + 4022);
    t14 = xsi_mem_cmp(t12, t9, 2U);
    if (t14 == 1)
        goto LAB10;

LAB15:    t15 = (t0 + 4024);
    t17 = xsi_mem_cmp(t15, t9, 2U);
    if (t17 == 1)
        goto LAB11;

LAB16:    t18 = (t0 + 4026);
    t20 = xsi_mem_cmp(t18, t9, 2U);
    if (t20 == 1)
        goto LAB12;

LAB17:
LAB13:    xsi_set_current_line(57, ng0);
    t2 = (t0 + 4044);
    t1 = (4U != 4U);
    if (t1 == 1)
        goto LAB27;

LAB28:    t6 = (t0 + 2532);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_delta(t6, 4U, 4U, 0LL);

LAB8:    goto LAB3;

LAB5:    t2 = (t0 + 684U);
    t6 = *((char **)t2);
    t7 = *((unsigned char *)t6);
    t8 = (t7 == (unsigned char)2);
    t1 = t8;
    goto LAB7;

LAB9:    xsi_set_current_line(53, ng0);
    t21 = (t0 + 4028);
    t23 = (4U != 4U);
    if (t23 == 1)
        goto LAB19;

LAB20:    t24 = (t0 + 2532);
    t25 = (t24 + 32U);
    t26 = *((char **)t25);
    t27 = (t26 + 40U);
    t28 = *((char **)t27);
    memcpy(t28, t21, 4U);
    xsi_driver_first_trans_delta(t24, 4U, 4U, 0LL);
    goto LAB8;

LAB10:    xsi_set_current_line(54, ng0);
    t2 = (t0 + 4032);
    t1 = (4U != 4U);
    if (t1 == 1)
        goto LAB21;

LAB22:    t6 = (t0 + 2532);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_delta(t6, 4U, 4U, 0LL);
    goto LAB8;

LAB11:    xsi_set_current_line(55, ng0);
    t2 = (t0 + 4036);
    t1 = (4U != 4U);
    if (t1 == 1)
        goto LAB23;

LAB24:    t6 = (t0 + 2532);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_delta(t6, 4U, 4U, 0LL);
    goto LAB8;

LAB12:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 4040);
    t1 = (4U != 4U);
    if (t1 == 1)
        goto LAB25;

LAB26:    t6 = (t0 + 2532);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_delta(t6, 4U, 4U, 0LL);
    goto LAB8;

LAB18:;
LAB19:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB20;

LAB21:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB22;

LAB23:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB24;

LAB25:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB26;

LAB27:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB28;

LAB29:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB30;

}

static void work_a_0440089032_3212880686_p_1(char *t0)
{
    unsigned char t1;
    char *t2;
    char *t3;
    unsigned char t4;
    unsigned char t5;
    char *t6;
    unsigned char t7;
    unsigned char t8;
    char *t9;
    char *t10;
    int t11;
    char *t12;
    char *t13;
    int t14;
    char *t15;
    int t17;
    char *t18;
    int t20;
    char *t21;
    unsigned char t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;

LAB0:    xsi_set_current_line(66, ng0);
    t2 = (t0 + 776U);
    t3 = *((char **)t2);
    t4 = *((unsigned char *)t3);
    t5 = (t4 == (unsigned char)2);
    if (t5 == 1)
        goto LAB5;

LAB6:    t1 = (unsigned char)0;

LAB7:    if (t1 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(75, ng0);
    t2 = (t0 + 4080);
    t1 = (4U != 4U);
    if (t1 == 1)
        goto LAB29;

LAB30:    t6 = (t0 + 2568);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_delta(t6, 0U, 4U, 0LL);

LAB3:    t2 = (t0 + 2480);
    *((int *)t2) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(67, ng0);
    t2 = (t0 + 960U);
    t9 = *((char **)t2);
    t2 = (t0 + 4052);
    t11 = xsi_mem_cmp(t2, t9, 2U);
    if (t11 == 1)
        goto LAB9;

LAB14:    t12 = (t0 + 4054);
    t14 = xsi_mem_cmp(t12, t9, 2U);
    if (t14 == 1)
        goto LAB10;

LAB15:    t15 = (t0 + 4056);
    t17 = xsi_mem_cmp(t15, t9, 2U);
    if (t17 == 1)
        goto LAB11;

LAB16:    t18 = (t0 + 4058);
    t20 = xsi_mem_cmp(t18, t9, 2U);
    if (t20 == 1)
        goto LAB12;

LAB17:
LAB13:    xsi_set_current_line(72, ng0);
    t2 = (t0 + 4076);
    t1 = (4U != 4U);
    if (t1 == 1)
        goto LAB27;

LAB28:    t6 = (t0 + 2568);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_delta(t6, 0U, 4U, 0LL);

LAB8:    goto LAB3;

LAB5:    t2 = (t0 + 868U);
    t6 = *((char **)t2);
    t7 = *((unsigned char *)t6);
    t8 = (t7 == (unsigned char)2);
    t1 = t8;
    goto LAB7;

LAB9:    xsi_set_current_line(68, ng0);
    t21 = (t0 + 4060);
    t23 = (4U != 4U);
    if (t23 == 1)
        goto LAB19;

LAB20:    t24 = (t0 + 2568);
    t25 = (t24 + 32U);
    t26 = *((char **)t25);
    t27 = (t26 + 40U);
    t28 = *((char **)t27);
    memcpy(t28, t21, 4U);
    xsi_driver_first_trans_delta(t24, 0U, 4U, 0LL);
    goto LAB8;

LAB10:    xsi_set_current_line(69, ng0);
    t2 = (t0 + 4064);
    t1 = (4U != 4U);
    if (t1 == 1)
        goto LAB21;

LAB22:    t6 = (t0 + 2568);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_delta(t6, 0U, 4U, 0LL);
    goto LAB8;

LAB11:    xsi_set_current_line(70, ng0);
    t2 = (t0 + 4068);
    t1 = (4U != 4U);
    if (t1 == 1)
        goto LAB23;

LAB24:    t6 = (t0 + 2568);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_delta(t6, 0U, 4U, 0LL);
    goto LAB8;

LAB12:    xsi_set_current_line(71, ng0);
    t2 = (t0 + 4072);
    t1 = (4U != 4U);
    if (t1 == 1)
        goto LAB25;

LAB26:    t6 = (t0 + 2568);
    t9 = (t6 + 32U);
    t10 = *((char **)t9);
    t12 = (t10 + 40U);
    t13 = *((char **)t12);
    memcpy(t13, t2, 4U);
    xsi_driver_first_trans_delta(t6, 0U, 4U, 0LL);
    goto LAB8;

LAB18:;
LAB19:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB20;

LAB21:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB22;

LAB23:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB24;

LAB25:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB26;

LAB27:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB28;

LAB29:    xsi_size_not_matching(4U, 4U, 0);
    goto LAB30;

}

static void work_a_0440089032_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    xsi_set_current_line(79, ng0);

LAB3:    t1 = (t0 + 1328U);
    t2 = *((char **)t1);
    t1 = (t0 + 2604);
    t3 = (t1 + 32U);
    t4 = *((char **)t3);
    t5 = (t4 + 40U);
    t6 = *((char **)t5);
    memcpy(t6, t2, 8U);
    xsi_driver_first_trans_fast_port(t1);

LAB2:    t7 = (t0 + 2488);
    *((int *)t7) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}


extern void work_a_0440089032_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0440089032_3212880686_p_0,(void *)work_a_0440089032_3212880686_p_1,(void *)work_a_0440089032_3212880686_p_2};
	xsi_register_didat("work_a_0440089032_3212880686", "isim/CascadeTBPart1_isim_beh.exe.sim/work/a_0440089032_3212880686.didat");
	xsi_register_executes(pe);
}
