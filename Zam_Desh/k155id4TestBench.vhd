--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:58:44 02/19/2016
-- Design Name:   
-- Module Name:   C:/Users/vm121/Zam_Desh/k155id4TestBench.vhd
-- Project Name:  Zam_Desh
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: k155id4
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY k155id4TestBench IS
END k155id4TestBench;
 
ARCHITECTURE behavior OF k155id4TestBench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT k155id4
    PORT(
         Ea : IN  std_logic;
         �ai : IN  std_logic;
         �b : IN  std_logic;
         �bi : IN  std_logic;
         a0 : IN  std_logic;
         a1 : IN  std_logic;
         y : OUT  std_logic_vector(0 downto 7)
        );
    END COMPONENT;
    

   --Inputs
   signal Ea : std_logic := '0';
   signal �ai : std_logic := '0';
   signal �b : std_logic := '0';
   signal �bi : std_logic := '0';
   signal a0 : std_logic := '0';
   signal a1 : std_logic := '0';

 	--Outputs
   signal y : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant <clock>_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: k155id4 PORT MAP (
          Ea => Ea,
          �ai => �ai,
          �b => �b,
          �bi => �bi,
          a0 => a0,
          a1 => a1,
          y => y
        );

   -- Clock process definitions
   <clock>_process :process
   begin
		<clock> <= '0';
		wait for <clock>_period/2;
		<clock> <= '1';
		wait for <clock>_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for <clock>_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
