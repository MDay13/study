--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:21:17 02/26/2016
-- Design Name:   
-- Module Name:   X:/students/vm121/Zamogilin/Zam_Desh/k155id4Test.vhd
-- Project Name:  Zam_Desh
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: k155id4
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY k155id4Test IS
END k155id4Test;
 
ARCHITECTURE behavior OF k155id4Test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT k155id4
    PORT(
         EA : IN  std_logic;
         EAI : IN  std_logic;
         eb : IN  std_logic;
         EBI : IN  std_logic;
         A : IN  std_logic_vector(1 downto 0);
         y : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal EA : std_logic := '0';
   signal EAI : std_logic := '0';
   signal eb : std_logic := '0';
   signal EBI : std_logic := '0';
	signal CLK : std_logic := '0';
   signal A : std_logic_vector(1 downto 0) := (others => '0');

 	--Outputs
   signal y : std_logic_vector(7 downto 0);
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: k155id4 PORT MAP (
          EA => EA,
          EAI => EAI,
          eb => eb,
          EBI => EBI,
          A => A,
          y => y
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 
		
		A <= "01";
		EB <='1';
		
		wait for CLK_period*10;
				
		EA <= '1';
		wait for CLK_period*10;
		EAI <= '0';
		wait for CLK_period*10;
		
		A <= "11";
		wait for CLK_period*10;
		
		EB <= '1';
		wait for CLK_period*10;
		EBI <= '1';
		wait for CLK_period*20;
		
		

      wait;
   end process;

END;
