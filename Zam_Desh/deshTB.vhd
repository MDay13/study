--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:33:54 02/26/2016
-- Design Name:   
-- Module Name:   C:/Users/vm121/Zam_Desh/deshTB.vhd
-- Project Name:  Zam_Desh
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: desh
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY deshTB IS
END deshTB;
 
ARCHITECTURE behavior OF deshTB IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT desh
    PORT(
         E : IN  std_logic;
         A : IN  std_logic_vector(1 downto 0);
         CLK : IN  std_logic;
         Y : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal E : std_logic := '0';
   signal A : std_logic_vector(1 downto 0) := (others => '0');
   signal CLK : std_logic := '0';

 	--Outputs
   signal Y : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant CLK_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: desh PORT MAP (
          E => E,
          A => A,
          CLK => CLK,
          Y => Y
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK_period*10;

      -- insert stimulus here 
		
		A <= "01";
		wait for CLK_period*10;
		A <="10";
		wait for CLK_period*10;
		A <= "11";
		wait for CLK_period*10;

      wait;
   end process;

END;
