----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:36:02 03/11/2016 
-- Design Name: 
-- Module Name:    k155id3 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity k155id3 is
Port(A0 : in STD_LOGIC;
	  A1 : in STD_LOGIC;
	  A2 : in STD_LOGIC;
	  A3 : in STD_LOGIC;
	  E0 : in STD_LOGIC;
	  E1 : in STD_LOGIC;
	  Q: out STD_LOGIC_VECTOR (15 downto 0));
end k155id3;

architecture Behavioral of k155id3 is
signal tmp: std_logic_vector(15 downto 0);
begin

	process (A0,A1,A2,A3,E0,E1)
	begin
		if (E0 = '0' and E1 = '0') then
			if (A0='0' and A1='0' and A2='0' and A3='0') then --1
			tmp <= "0000000000000001";
			elsif (A0='1' and A1='0' and A2='0' and A3='0') then --2
			tmp <= "0000000000000010";
			elsif (A0='0' and A1='1' and A2='0' and A3='0') then --3
			tmp <= "0000000000000100";
			elsif (A0='1' and A1='1' and A2='0' and A3='0') then --4
			tmp <= "0000000000001000";
			elsif (A0='0' and A1='0' and A2='0' and A3='0') then --5
			tmp <= "0000000000010000";
			elsif (A0='1' and A1='0' and A2='1' and A3='0') then --6
			tmp <= "0000000000100000";
			elsif (A0='0' and A1='1' and A2='1' and A3='0') then --7
			tmp <= "0000000001000000";
			elsif (A0='1' and A1='1' and A2='1' and A3='0') then --8
			tmp <= "0000000010000000";
			elsif (A0='0' and A1='0' and A2='1' and A3='1') then --9
			tmp <= "0000000100000000";
			elsif (A0='1' and A1='0' and A2='0' and A3='1') then --10
			tmp <= "0000001000000000";
			elsif (A0='0' and A1='1' and A2='0' and A3='1') then --11
			tmp <= "0000010000000000";
			elsif (A0='1' and A1='1' and A2='0' and A3='1') then --12
			tmp <= "0000100000000000";
			elsif (A0='0' and A1='0' and A2='1' and A3='1') then --13
			tmp <= "0001000000000000";
			elsif (A0='1' and A1='0' and A2='1' and A3='1') then --14
			tmp <= "0010000000000000";
			elsif (A0='0' and A1='1' and A2='1' and A3='1') then --15
			tmp <= "0100000000000000";
			elsif (A0='1' and A1='1' and A2='1' and A3='1') then --16
			tmp <= "1000000000000000";
			end if;
		else
		tmp <= "0000000000000000";
		end if;
		
	end process;
	
	Q <= not tmp;

end Behavioral;

